﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour
{
	
	//Movement
	public float speed;
	public float jump;
	public float moveVelocity;
	public Transform groundCheck;
	public LayerMask whatIsGround;
	public float groundCheckRadius;

	private Animator animator;
	private bool grounded;

	void Start () 
	{
		animator = GetComponent<Animator>();
	}

	void FixedUpdate() {
		grounded = Physics2D.OverlapCircle (groundCheck.position, groundCheckRadius, whatIsGround);
		animator.SetBool("onGround", grounded);
	}

	void Update ()
	{

		moveVelocity = 0f;

		//Jumping
		if (Input.GetKeyDown (KeyCode.Space) || Input.GetKeyDown (KeyCode.UpArrow))
		{
			if(grounded)
			{
				GetComponent<Rigidbody2D> ().velocity = new Vector2 (GetComponent<Rigidbody2D> ().velocity.x, jump);
				animator.SetTrigger("playerJump");
			}
		}
		
		//Left Right Movement
		if (Input.GetKey (KeyCode.LeftArrow) || Input.GetKey (KeyCode.A)) {
			moveVelocity = -speed;
			animator.SetTrigger ("playerRun");
		}  
		else if (Input.GetKey (KeyCode.Space) || Input.GetKey (KeyCode.RightShift)) {
			moveVelocity = -speed;
			animator.SetTrigger ("playerAttack");
		}
		else if (Input.GetKey (KeyCode.RightArrow) || Input.GetKey (KeyCode.D)) {
			moveVelocity = speed;
			animator.SetTrigger ("playerRun");
		} 
		else {
			animator.SetTrigger("playerIdle");
		}

		GetComponent<Rigidbody2D> ().velocity = new Vector2 (moveVelocity, GetComponent<Rigidbody2D> ().velocity.y);

		if(GetComponent<Rigidbody2D>().velocity.x > 0) {
			transform.localScale = new Vector3(4.03f, 4.03f, 1f);
		}
		else if(GetComponent<Rigidbody2D>().velocity.x < 0) {
			transform.localScale = new Vector3(-4.03f, 4.03f, 1f);
		}
	}
	//Check if Grounded
	void OnTriggerEnter2D()
	{
		grounded = true;
	}
	void OnTriggerExit2D()
	{
		grounded = false;
	}
}

	